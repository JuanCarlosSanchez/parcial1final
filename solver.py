import time

class Nodo:
    def __init__(self,datos,l,fval, direcciones, listaAnteriores):
        self.datos = datos
        self.l = l
        self.fval = fval
        self.direcciones = direcciones
        self.listaAnteriores = listaAnteriores 

    def generarHijos(self):
        hijos = []
        ceros = []
        n = len(self.datos)

        for i in range(n):
            for j in range(n):
                if self.datos[i][j] == "0":
                    ceros.append([i,j])

        for i in ceros:
            desplazados = [[i[0]+1,i[1]],[i[0]-1,i[1]],[i[0],i[1]+1],[i[0],i[1]-1]]
            for j in desplazados:
                
                if not (j[0] >= 0 and j[0] < n and j[1] >= 0 and j[1] < n):
                    continue

                if i[0] < j[0]:
                    direcciones = ("Abajo")
                elif i[0] > j[0]:
                    direcciones = ("Arriba")
                elif i[1] > j[1]:
                    direcciones = ("Izquierda")
                elif i[1] < j[1]:
                    direcciones = ("Derecha")
                else:
                    pass 
                hijo = self.validar(i[0],i[1],j[0],j[1])
                hijo = Nodo(hijo,self.l+1,0,direcciones,self)
                hijos.append(hijo)
        return hijos 


    def validar(self,y0,x0,y1,x1):
        previos = []

        for i in self.datos:
            t = []
            for j in i:
                t.append(j)
            previos.append(t)

        tmp = previos[y1][x1]
        previos[y1][x1] = previos[y0][x0]
        previos[y0][x0] = tmp
        return previos
            

class Rompecocos:
    def __init__(self,n, start, goal):
        self.n = n
        self.open = []
        self.closed = []
        self.start = start
        self.goal = goal

    def f(self,start,goal):
        return self.h_manhattan(start.datos,goal)+start.l

    def h_manhattan(self,start,goal):
        suma = 0
        coordenadas = {}
        for i in range(self.n):
            for j in range(self.n):
                if start[i][j] in coordenadas.keys():
                    coordenadas[start[i][j]].append([i,j])
                elif start[i][j] != '0':
                    coordenadas[start[i][j]] = []
                    coordenadas[start[i][j]].append([i,j])

                if goal[i][j] in coordenadas.keys():
                    coordenadas[goal[i][j]].append([i,j])
                elif goal[i][j] != '0':
                    coordenadas[goal[i][j]] = []
                    coordenadas[goal[i][j]].append([i,j])

        for key ,value in coordenadas.items():
            suma += abs(value[0][0]-value[1][0]) + abs(value[0][1]-value[1][1])
        return suma
        

    def resolver(self):

        start = Nodo(self.start,0,0,None,None)
        start.fval = self.f(start,goal)
        self.open.append(start)
        cont = 0

        while len(self.open) > 0:
            actual = self.open[0]
            if self.h_manhattan(actual.datos,goal) == 0:
                iteraciones = []
                while actual.listaAnteriores != None:
                    iteraciones.append(actual.direcciones)
                    actual = actual.listaAnteriores
                print(iteraciones[::-1])
                print("Iteraciones:{0}".format(cont))
                break

            for hijo in actual.generarHijos():
                is_closed = False
                for closed_node in self.closed:
                    if closed_node.datos == hijo.datos:
                        is_closed = True
                        break
                if(is_closed):
                    continue

                if hijo not in self.open:
                    hijo.f_val = self.f(hijo,goal)
                    self.open.append(hijo)

            self.closed.append(actual)
            del self.open[0]
            self.open.sort(key = lambda x:x.f_val,reverse=False)
            cont += 1


n = int(input("N: "))
print("Start:")
start = []
for i in range(0,n):
    temp = input().split(",")
    start.append(temp)
print("Goal:")
goal = []
for i in range(0,n):
    temp = input().split(",")
    goal.append(temp)
rompecocos = Rompecocos(n, start, goal)
inicio = time.time ()
rompecocos.resolver()
fin = time.time ()
print("Tiempo:")
print(fin-inicio)